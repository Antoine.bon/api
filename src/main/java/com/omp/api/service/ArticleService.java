package com.omp.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.omp.api.model.Articles;
import com.omp.api.repository.ArticlesRepository;

import lombok.Data;

@Data
@Service
public class ArticlesService {

    @Autowired
    private ArticlesRepository ArticlesRepository;

    public Optional<Articles> getArticles(final Long id) {
        return ArticlesRepository.findById(id);
    }

    public Iterable<Articles> getArticless() {
        return ArticlesRepository.findAll();
    }

    public void deleteArticles(final Long id) {
        ArticlesRepository.deleteById(id);
    }

    public Articles saveArticles(Articles Articles) {
        Articles savedArticles = ArticlesRepository.save(Articles);
        return savedArticles;
    }

}