package com.omp.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.omp.api.model.Articles;

@Repository
public interface ArticlesRepository extends CrudRepository<Articles, Long> {

}