package com.omp.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.omp.api.model.Articles;
import com.omp.api.service.ArticlesService;

@RestController
public class ArticlesController {

	@Autowired
	private ArticlesService ArticlesService;
	
	/**
	 * Create - Add a new Articles
	 */
	@PostMapping("/Articles")
	public Articles createArticles(@RequestBody Articles Articles) {
		return ArticlesService.saveArticles(Articles);
	}
	
	
	/**
	 * Read - Get all Articless
	 */
	@GetMapping("/Articless")
	public Iterable<Articles> getArticless() {
		return ArticlesService.getArticless();
	}
	
	/**
	 * Read - Get one Articles 
	 */
	@GetMapping("/Articles/{id}")
	public Articles getArticles(@PathVariable("id") final Long id) {
		Optional<Articles> Articles = ArticlesService.getArticles(id);
		if(Articles.isPresent()) {
			return Articles.get();
		} else {
			return null;
		}
	} 

	
	/**
	 * Update - Update an existing Articles
	 * @param id - The id of the Articles to update
	 * @param Articles - The Articles object updated
	 * @return
	 
	 */
	@PutMapping("/Articles/{id}")
	public Articles updateArticles(@PathVariable("id") final Long id, @RequestBody Articles Articles) {
		Optional<Articles> e = ArticlesService.getArticles(id);
		if(e.isPresent()) {
			Articles currentArticles = e.get();
			
			String firstName = Articles.getFirstname();
			if(firstName != null) {
				currentArticles.setFirstname(firstName);
			}
			String lastName = Articles.getFirstname();
			if(lastName != null) {
				currentArticles.setFirstname(lastName);;
			}
			String mail = Articles.getEmail();
			if(mail != null) {
				currentArticles.setEmail(mail);
			}
			String password = Articles.getPassword();
			if(password != null) {
				currentArticles.setPassword(password);;
			}
			ArticlesService.saveArticles(currentArticles);
			return currentArticles;
		} else {
			return null;
		}
	}
	
	
	/**
	 * Delete - Delete an Articles
	 * @param id - The id of the Articles to delete
	 */
	@DeleteMapping("/Articles/{id}")
	public void deleteArticles(@PathVariable("id") final Long id) {
		ArticlesService.deleteArticles(id);
	}

}
